import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.http.client.ClientProtocolException;
import org.jclouds.rest.ResourceNotFoundException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import eu.specs.datamodel.broker.BrokerTemplateBean;
import eu.specs.datamodel.broker.ClusterNode;
import eu.specs.datamodel.broker.NodeCredential;
import eu.specs.datamodel.broker.NodeCredentialsManager;
import eu.specs.datamodel.broker.NodesInfo;
import eu.specs.datamodel.broker.ProviderCredential;
import eu.specs.datamodel.broker.ProviderCredentialsManager;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.project.enforcement.broker.ChefServiceImpl;
import eu.specs.project.enforcement.broker.CloudService;
import eu.specs.project.enforcement.broker.CloudServiceEucalyptus;
import eu.specs.project.enforcement.contextlistener.ContextListener;

public class Main {
	private static ChefServiceImpl chefService;
	private static String databagId = "a7452816dbd74eab928fe109b9516056";
	//	public static PROVIDER current_provider= PROVIDER.EUCALYPTUS;

//	private static String plan="{ \"plan_id\": \"1154982\", \"plan_act_id\": \"\", \"sla_id\": \"\", \"supply_chain_id\": \"\", \"creation_time\": \"\", \"monitoring_core_ip\": \"192.168.1.150\", \"monitoring_core_port\": \"5000\", \"iaas\": { \"provider\": \"aws-ec2\", \"zone\": \"us-east-1\", \"appliance\": \"ami-ff0e0696\", \"hardware\": \"t1.micro\" }, \"pools\": [ { \"pool_name\": \"\", \"pool_seq_num\": \"\", \"nodes_access_credentials_reference\": \"\", \"vms\": [ { \"vm_seq_num\": \"\", \"public_ip\": \"filled by Implementation\", \"components\": [ { \"component_id\": \"ha-proxy-node\", \"cookbook\": \"specs-enforcement-webpool\", \"recipe\": \"haproxy\", \"implementation_step\": \"1\", \"acquire_public_ip\": \"true\", \"private_ips_count\": \"1\", \"firewall\": { \"incoming\": { \"source_ips\": [ \"0.0.0.0/0\" ], \"source_nodes\": [], \"interface\": \"public\", \"proto\": [ \"TCP\" ], \"port_list\": [ \"22\", \"80\", \"443\" ] }, \"outcoming\": { \"destination_ips\": [ \"0.0.0.0/0\" ], \"destination_nodes\": [ \"\" ], \"interface\": \"public,private:1\", \"proto\": [ \"TCP\" ], \"port_list\": [ \"*\" ] } }, \"private_ips\": [ \"172.31.58.95\" ] } ] }, { \"vm_seq_num\": \"\", \"public_ip\": \"$$ filled by Implementation\", \"components\": [ { \"component_id\": \"apache-node1\", \"cookbook\": \"specs-enforcement-webpool\", \"recipe\": \"apache\", \"implementation_step\": \"1\", \"acquire_public_ip\": \"false\", \"private_ips_count\": \"1\", \"firewall\": { \"incoming\": { \"source_ips\": [ \"0.0.0.0/0\" ], \"source_nodes\": [ \"ha-proxy\" ], \"interface\": \"private:1\", \"proto\": [ \"TCP\" ], \"port_list\": [ \"22\", \"80\", \"443\" ] }, \"outcoming\": { \"destination_ips\": [ \"\" ], \"destination_nodes\": [], \"interface\": \"private:1\", \"proto\": [ \"TCP\" ], \"port_list\": [ \"*\" ] } }, \"private_ips\": [ \"172.31.60.225\" ] } ] }, { \"vm_seq_num\": \"\", \"public_ip\": \"$$ filled by Implementation\", \"components\": [ { \"component_id\": \"apache-node-2\", \"cookbook\": \"specs-enforcement-webpool\", \"recipe\": \"apache\", \"implementation_step\": \"1\", \"acquire_public_ip\": \"false\", \"private_ips_count\": \"1\", \"firewall\": { \"incoming\": { \"source_ips\": [ \"\" ], \"source_nodes\": [ \"ha-proxy\" ], \"interface\": \"private:1\", \"proto\": [ \"TCP\" ], \"port_list\": [ \"22\", \"80\", \"443\" ] }, \"outcoming\": { \"destination_ips\": [ \"\" ], \"destination_nodes\": [ \"ha-proxy\" ], \"interface\": \"private:1\", \"proto\": [ \"TCP\" ], \"port_list\": [ \"*\" ] } }, \"private_ips\": [ \"172.31.51.223\" ] } ] }, { \"vm_seq_num\": \"\", \"public_ip\": \"$$ filled by Implementation\", \"components\": [ { \"component_id\": \"nginx-node\", \"cookbook\": \"specs-enforcement-webpool\", \"recipe\": \"nginx\", \"implementation_step\": \"1\", \"acquire_public_ip\": \"false\", \"private_ips_count\": \"1\", \"firewall\": { \"incoming\": { \"source_ips\": [ \"\" ], \"source_nodes\": [ \"ha-proxy\" ], \"interface\": \"private:1\", \"proto\": [ \"TCP\" ], \"port_list\": [ \"22\", \"80\", \"443\" ] }, \"outcoming\": { \"destination_ips\": [ \"\" ], \"destination_nodes\": [ \"ha-proxy\" ], \"interface\": \"private:1\", \"proto\": [ \"TCP\" ], \"port_list\": [ \"*\" ] } }, \"private_ips\": [ \"172.31.48.150\" ] } ] } ] } ], \"slos\": [ { \"slo_id\": \"\", \"capability\": \"\", \"metric_id\": \"\", \"unit_type\": \"\", \"unit\": \"\", \"value\": \"\", \"operator\": \"\", \"value1\": \"\", \"value2\": \"\", \"interval_step\": \"\", \"importance_level\": \"LOW\" } ], \"measurements\": [ { \"msr_id\": \"\", \"msr_description\": \"\", \"frequency\": \"\", \"metrics\": [ \"\" ], \"monitoring_event\": { \"event_id\": \"\", \"event_description\": \"\", \"event_type\": \"VIOLATION\", \"condition\": { \"operator\": \"\", \"threshold\": \"\" } } } ] }";
	private static String plan="{\"plan_id\": \"1154982\",\"plan_act_id\": \"\",\"sla_id\": \"\",\"supply_chain_id\": \"\",\"creation_time\": \"\",\"monitoring_core_ip\": \"192.168.1.150\",\"monitoring_core_port\": \"5000\",\"iaas\": {\"provider\": \"ec2\",\"zone\": \"ieat01\",\"appliance\": \"eucalyptus/emi-65c3e382\",\"hardware\": \"c1.medium\"},\"pools\": [{\"pool_name\": \"\",\"pool_seq_num\": \"\",\"nodes_access_credentials_reference\": \"\",\"vms\": [{\"vm_seq_num\": \"\",\"public_ip\": \"filled by Implementation\",\"components\": [{\"component_id\": \"ha-proxy-node\",\"cookbook\": \"applications\",\"recipe\": \"metric-catalogue-app\",\"implementation_step\": \"1\",\"acquire_public_ip\": \"true\",\"private_ips_count\": \"1\",\"firewall\": {\"incoming\": {\"source_ips\": [\"0.0.0.0/0\"],\"source_nodes\": [],\"interface\": \"public\",\"proto\": [\"TCP\"],\"port_list\": [\"22\",\"80\",\"443\"]},\"outcoming\": {\"destination_ips\": [\"0.0.0.0/0\"],\"destination_nodes\": [\"\"],\"interface\": \"public,private:1\",\"proto\": [\"TCP\"],\"port_list\": [\"*\"]}},\"private_ips\": [\"172.31.58.95\"]}]},{\"vm_seq_num\": \"\",\"public_ip\": \"$$ filled by Implementation\",\"components\": [{\"component_id\": \"nginx-node\",\"cookbook\": \"applications\",\"recipe\": \"metric-catalogue-app\",\"implementation_step\": \"1\",\"acquire_public_ip\": \"false\",\"private_ips_count\": \"1\",\"firewall\": {\"incoming\": {\"source_ips\": [\"\"],\"source_nodes\": [\"ha-proxy\"],\"interface\": \"private:1\",\"proto\": [\"TCP\"],\"port_list\": [\"22\",\"80\",\"443\"]},\"outcoming\": {\"destination_ips\": [\"\"],\"destination_nodes\": [\"ha-proxy\"],\"interface\": \"private:1\",\"proto\": [\"TCP\"],\"port_list\": [\"*\"]}},\"private_ips\": [\"172.31.48.150\"]}]}]}],\"slos\": [{\"slo_id\": \"\",\"capability\": \"\",\"metric_id\": \"\",\"unit_type\": \"\",\"unit\": \"\",\"value\": \"\",\"operator\": \"\",\"value1\": \"\",\"value2\": \"\",\"interval_step\": \"\",\"importance_level\": \"LOW\"}],\"measurements\": [{\"msr_id\": \"\",\"msr_description\": \"\",\"frequency\": \"\",\"metrics\": [\"\"],\"monitoring_event\": {\"event_id\": \"\",\"event_description\": \"\",\"event_type\": \"VIOLATION\",\"condition\": {\"operator\": \"\",\"threshold\": \"\"}}}]}";
	
	//	public enum PROVIDER {
	//		AMAZON, EUCALYPTUS 
	//	}


	public static void main(String[] args) {
		chefService = new  ChefServiceImpl(
				null,
				null,
				null,
				null,
				null);

		//		current_provider=PROVIDER.EUCALYPTUS;

		//		System.out.println("databag: "+chefService.getDatabagItem("implementation_plans", "a7452816dbd74eab928fe109b9516056").toString());

		//		uploadDatabag();
		//		acquireAmazonVms();

		
		ImplementationPlan myplan;
		try {
			myplan = parsePlan();
			if(myplan!=null)
				acquireEucalyptusVMs(myplan);
			else
				System.out.println("il piano e' nullo");
		} catch (IOException e) {
			e.printStackTrace();
		}



	}

	private static String[] prepareEnvironmentScript(String appliance) {
		String[] script= {
				//				"zypper -n --gpg-auto-import-keys in curl" ,
				//				"curl -O http://curl.haxx.se/ca/cacert.pem",
				//				"mv cacert.pem /etc/ca-certificates/",
				//				"touch .bashrc",
				//				"echo 'export CURL_CA_BUNDLE=/etc/ca-certificates/cacert.pem' > .bashrc",
				"/opt/mos-chef-client/bin/check-chef-server.sh"
		};
		return script;
	}


	private static ImplementationPlan parsePlan() throws IOException {

		ObjectMapper mapper = new ObjectMapper();
		ImplementationPlan myplan;
		try {
			myplan = mapper.readValue(plan, ImplementationPlan.class);
			System.out.println("myplan.getId(): "+myplan.getId());
			return myplan;
		} catch (JsonParseException e) {
			e.printStackTrace();
			throw e;
		} catch (JsonMappingException e) {
			e.printStackTrace();
			throw e;
		}
	}


	private static void acquireAmazonVms(){
		// inserisco i valori che descrivono il provider e le sue credenziali
		BrokerTemplateBean bean = new BrokerTemplateBean();
		bean.setAppliance("us-east-1/ami-ff0e0696");
		bean.setHw("t1.micro");
		bean.setProvider("aws-ec2");
		bean.setZone("us-east-1");

		ProviderCredential provCred = new ProviderCredential("AKIAJUXYVK7J5IUFDKUA","XbCQCfWYXzcD82K/Cx20XY8TSHUwxSo/atvpIyLF");
		ProviderCredentialsManager.add("provider", provCred);
		CloudService cloudservice = new CloudService(bean.getProvider(), "root", provCred);		

		java.util.Date date= new java.util.Date();	
		System.out.println(new Timestamp(date.getTime())+" | VMs creating");	


		//		InstanceDescriptor descr = new InstanceDescriptor(bean.appliance, bean.zone, bean.hw);

		String group = "sla-1";

		ContextListener cc = new ContextListener();
		NodeCredential myCred = cc.brokerCredentials();
		System.out.println("le credenziali sono: "+(myCred==null ? "nulle":"non nulle"));
		System.out.println("public key: "+myCred.getPublickey());

		NodeCredential cred = new NodeCredential("specs.123456", 
				myCred.getPublickey(), 
				myCred.getPrivatekey());
		NodeCredentialsManager.add("def", cred);

		try {
			NodesInfo nodesInfo = cloudservice.createNodesInGroup(group, 1 , bean,
					NodeCredentialsManager.getCredentials("def"),22,80,11211, 9390,1514);

			java.util.Date myDate= new java.util.Date();	
			System.out.println(new Timestamp(myDate.getTime())+" | VMs created");

			//-------Update Implementation-Info with nodes info----------------
			//			info.status=Status.VMs_Acquired;
			List<ClusterNode> nodes=nodesInfo.getNodes();

			for(ClusterNode node : nodes){

				String s=node.getPrivateIP();
				System.out.println("ip privato acquisito: "+s);
				//				info.private_IP_Address.add(s);
				//				info.public_IP_Address.add(node.getPublicIP());

			}

			//-----------Preparing VMs : install curl--------------------
			java.util.Date mydatenew= new java.util.Date();	
			System.out.println(new Timestamp(mydatenew.getTime())+" | VMs preparing");

			Thread[] threads = new Thread[nodes.size()];
			for (int i = 0; i < threads.length; i++) {
				threads[i] = cloudservice.new ExecuteIstructionsOnNode("root",nodes.get(i),prepareEnvironmentScript(""), cred.getPrivatekey(),true,cloudservice);
				threads[i].start();
			}

			for (int i = 0; i < threads.length; i++) {
				try {
					threads[i].join();
				} catch (InterruptedException ignore) {}
			}

			java.util.Date date1= new java.util.Date();	
			System.out.println(new Timestamp(date1.getTime())+" | VMs prepared");


			//			info.status=Status.VMs_Prepared;


			//attributo del nodo che viene letto dalla ricetta
			String attribute= "{\"implementation_plan_id\":\""+databagId+"\"}";		


			java.util.Date date4= new java.util.Date();	   
			System.out.println(new Timestamp(date4.getTime())+" | chef node bootstrapping");


			chefService.bootstrapChef(group, nodesInfo, cloudservice,attribute);

			java.util.Date date5= new java.util.Date();	
			System.out.println(new Timestamp(date5.getTime())+" | chef node bootstrapped");

			//			info.status=Status.Chef_Node_Bootstrapped;

			//			for(ClusterNode node : nodes)  info.chefNodesName.add(group+"-"+node.getPrivateIP());


			List<List<String>> recipes=new ArrayList<List<String>> ();
			List <String> temp = new ArrayList<String>();
			temp.add("specs-enforcement-webpool::nginx");
			recipes.add(temp);

			//---------------------Executing recipes on nodes ------------------------------------------
			for(int i=0;i<nodes.size();i++ ){


				java.util.Date date6= new java.util.Date();	
				System.out.println(new Timestamp(date6.getTime())+" | executing recipes: "+Arrays.toString(recipes.get(i).toArray())+" on node "+group+"-"+nodes.get(i).getPrivateIP()+" publicIP:"+nodes.get(i).getPublicIP());				

				threads[i] = chefService.new ExecuteRecipesOnNode(nodes.get(i), recipes.get(i), group,myCred.getPrivatekey(), cloudservice);
				threads[i].start();

				//				info.recipes.add(InfoRecipes(recipes.get(i)));

			}

			for(int i=0;i<nodes.size();i++ ){
				try {
					threads[i].join();
				} catch (InterruptedException ignore) {}
			}

			if(true){
				java.util.Date date7= new java.util.Date();		
				System.out.println(new Timestamp(date7.getTime())+" | recipes completed");
			}

			//			info.status=Status.Recipes_completed;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	private static void uploadDatabag(){
		String databagId = UUID.randomUUID().toString().replace("-","");
		try {
			chefService.uploadDatabagItem("implementation_test",  databagId, "{\"plan_id\":\"1\",\"value\":\"30\"}");
		} catch (ResourceNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static void acquireEucalyptusVMs(ImplementationPlan plan){
		//		// inserisco i valori che descrivono il provider e le sue credenziali
		//		BrokerTemplateBean bean = new BrokerTemplateBean();
		//		bean.appliance="eucalyptus/emi-65c3e382";
		//		bean.hw="c1.medium";
		//		//il provider di eucalyptus e' ec2, mentre quelo di amazon e' aws-ec2
		//		bean.provider="ec2";
		//		bean.zone="ieat01";

		//		bean.appliance=plan.getIaas().getZone()+"/"+plan.getIaas().getAppliance();
		//		bean.hw=plan.getIaas().getHardware();
		//		bean.provider=plan.getIaas().getProvider();
		//		bean.zone=plan.getIaas().getZone();

		//AMAZON
		//		ProviderCredential provCred = new ProviderCredential("AKIAJUXYVK7J5IUFDKUA","XbCQCfWYXzcD82K/Cx20XY8TSHUwxSo/atvpIyLF");
		//EUCALYPTUS
		//		ProviderCredential provCred = new ProviderCredential("AKIE5G1IFU0ZOWY6VQOQ","Hx8aDZ41f4WV55xmpGiHXLyzsWvDBy9UCvk2n4Ri");

		ContextListener cc = new ContextListener();
				BrokerTemplateBean bean = cc.getDefaultBrokerTemplateBean();
//		BrokerTemplateBean bean = new BrokerTemplateBean();
//		bean.setAppliance(plan.getIaas().getZone()+"/"+plan.getIaas().getAppliance());
//		bean.setHw(plan.getIaas().getHardware());
//		bean.setProvider(plan.getIaas().getProvider());
//		bean.setZone(plan.getIaas().getZone());


		ProviderCredential provCred = cc.getDefaultProviderCredentials();
		ProviderCredentialsManager.add("provider", provCred);
		CloudServiceEucalyptus cloudservice = new CloudServiceEucalyptus(bean.getProvider(), "root", provCred);		

		java.util.Date date= new java.util.Date();	
		System.out.println(new Timestamp(date.getTime())+" | VMs creating");	


		//		InstanceDescriptor descr = new InstanceDescriptor(bean.getAppliance(), bean.getZone(), bean.getHw());

		String group = "sla-"+plan.getId();


		NodeCredential myCred = cc.brokerCredentials();
		//		System.out.println("le credenziali sono: "+(myCred==null ? "nulle":"non nulle"));
		//		System.out.println("public key: "+myCred.getPublickey());
		//		System.out.println("private key: "+myCred.getPrivatekey());

		NodeCredential cred = new NodeCredential(
				myCred.getPublickey(), 
				myCred.getPrivatekey());
		NodeCredentialsManager.add("def", cred);

		int vmsNumber = plan.getPools().get(0).getVms().size();


		try {
			NodesInfo nodesInfo = cloudservice.createNodesInGroup(group, vmsNumber , bean,
					NodeCredentialsManager.getCredentials("def"), 22, 80, 8080, 11211, 9390, 1514);

			//			NodesInfo nodesInfo = cloudservice.createNodesInGroup(group, 1 , descr,
			//					NodeCredentialsManager.getCredentials("def"),22,80,11211, 9390,1514);

			java.util.Date myDate= new java.util.Date();	
			System.out.println(new Timestamp(myDate.getTime())+" | VMs created");

			//-------Update Implementation-Info with nodes info----------------
			//			info.status=Status.VMs_Acquired;
			List<ClusterNode> nodes=nodesInfo.getNodes();

			for(ClusterNode node : nodes){

				String s=node.getPrivateIP();
				System.out.println("ip privato acquisito: "+s);
				System.out.println("ip pubblico acquisito: "+node.getPublicIP());
				System.out.println("node id: "+node.getId());
				//				info.private_IP_Address.add(s);
				//				info.public_IP_Address.add(node.getPublicIP());

			}

			//-----------Preparing VMs : install curl--------------------
			java.util.Date mydatenew= new java.util.Date();	
			System.out.println(new Timestamp(mydatenew.getTime())+" | VMs preparing");

			Thread[] threads = new Thread[nodes.size()];
			for (int i = 0; i < threads.length; i++) {
				threads[i] = cloudservice.new executeIstructionsOnNode("root",nodes.get(i),prepareEnvironmentScript(""), cred.getPrivatekey(), true, cloudservice);
				threads[i].start();
			}

			for (int i = 0; i < threads.length; i++) {
				try {
					threads[i].join();
				} catch (InterruptedException ignore) {}
			}


			java.util.Date date1= new java.util.Date();	
			System.out.println(new Timestamp(date1.getTime())+" | VMs prepared");


			//---------------------Executing recipes on nodes ------------------------------------------
			for(int i=0;i<nodes.size();i++ ){

				List<List<String>> recipes=new ArrayList<List<String>> ();
				List <String> temp = new ArrayList<String>();
				temp.add(plan.getPools().get(0).getVms().get(i).getComponents().get(0).getCookbook()
						+"::"+plan.getPools().get(0).getVms().get(i).getComponents().get(0).getRecipe());
				//				temp.add("applications::metric-catalogue-app");
				//				temp.add("specs-enforcement-webpool::nginx");
				recipes.add(temp);

				java.util.Date date6= new java.util.Date();	
				System.out.println(new Timestamp(date6.getTime())+" | executing recipes: "+Arrays.toString(recipes.get(0).toArray())+" on node "+group+"-"+nodes.get(i).getPrivateIP()+" publicIP:"+nodes.get(i).getPublicIP());				
				final int current = i;

				threads[i]= new Thread() {

					@Override
					public void run() {
						super.run();
						String rec = "";
						for(int i=0;i<recipes.get(0).size();i++){
							rec=recipes.get(0).get(i);
						}
						final String recipes = rec;
						String path = ContextListener.CHEF_SERVER_ENDPOINT.replace("/organizations/", "").replace("https:", "http:")+
								":81/mos/cgi/mos-chef-server-core/chef-server-apply-recipe.cgi?token="+
								CloudServiceEucalyptus.EUCALYPTUS_CHEF_SERVER_TOKEN+
								"&node="+nodes.get(current).getId().replace("eucalyptus/", "")+"&recipe="+recipes;

						System.out.println("path ricette chef e': "+path);
						String res;
						try {
							res = ContextListener.doGet(path);
							//						String res = ContextListener.doGetUncheckSSL(path);
							JsonParser parser = new JsonParser();
							JsonObject o = (JsonObject)parser.parse(res);

							//							if(o.get("message").equals("ok"))
							//								System.out.println("recipe executed properly");
							//							else{
							//								System.out.println("ERROR in recipe execution: "+res);
							long d = 5000;
							for(int i=0;i<10;i++){
								try {
									if(i!=0){
										System.out.println("prima di sleep: "+i);
										sleep(d);
										System.out.println("dopo sleep: "+i);
									}

									String res2 = ContextListener.doGet(path);
									System.out.println("res della get: "+res2);
									JsonObject obj = (JsonObject)parser.parse(res2);
									System.out.println("obj della get: "+obj.toString());
									System.out.println("obj.get message: "+obj.get("message"));
									if(obj.get("message").toString().equals("ok")
											|| obj.get("message").toString().equals("\"ok\"")){
										System.out.println("recipe executed properly");
										break;
									}
								} catch (Exception e1) {
									System.out.println("si è verificato un catch nel catch di ExecuteIstructionsOnNode: "+i);
									e1.printStackTrace();
								}
							}
							//							}
						} catch (ClientProtocolException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}


				};

				//				threads[i]= new Thread(new Runnable() {
				//
				//					@Override
				//					public void run() {
				//						String rec = "";
				//						for(int i=0;i<recipes.get(current).size();i++){
				//							rec=recipes.get(current).get(i);
				//						}
				//						final String recipes = rec;
				//						String path = ContextListener.CHEF_SERVER_ENDPOINT.replace("/organizations/", "").replace("https:", "http:")+
				//								":81/mos/cgi/mos-chef-server-core/chef-server-apply-recipe.cgi?token="+
				//								CloudServiceEucalyptus.EUCALYPTUS_CHEF_SERVER_TOKEN+
				//								"&node="+nodes.get(current).getId().replace("eucalyptus/", "")+"&recipe="+recipes;
				//
				//						System.out.println("path ricette chef e': "+path);
				//						String res;
				//						try {
				//							res = ContextListener.doGet(path);
				//							//						String res = ContextListener.doGetUncheckSSL(path);
				//							JsonParser parser = new JsonParser();
				//							JsonObject o = (JsonObject)parser.parse(res);
				//
				//							if(o.get("message").equals("ok"))
				//								System.out.println("recipe executed properly");
				//							else{
				//								System.out.println("ERROR in recipe execution: "+res);
				//								long d = 10000;
				//								for(int i=0;i<10;i++){
				//									try {
				//										System.out.println("prima di sleep: "+i);
				//										sleep(d);
				//										System.out.println("dopo sleep: "+i);
				//
				//										compute.executeScriptOnNode(user, node,istructions,privateKey,sudo);
				//										break;
				//									} catch (Exception e1) {
				//										System.out.println("si è verificato un catch nel catch di ExecuteIstructionsOnNode: "+i);
				//										e1.printStackTrace();
				//									}
				//								}
				//							}
				//						} catch (ClientProtocolException e) {
				//							e.printStackTrace();
				//						} catch (IOException e) {
				//							e.printStackTrace();
				//						}
				//					}
				//				});
				//				threads[i] = chefService.new ExecuteRecipesOnNode(nodes.get(i), recipes.get(0), group,myCred.getPrivatekey(), cloudservice);
				threads[i].start();

				//				info.recipes.add(InfoRecipes(recipes.get(i)));
			}

			for(int i=0;i<nodes.size();i++ ){
				try {
					threads[i].join();
				} catch (InterruptedException ignore) {}
			}

			if(true){
				java.util.Date date7= new java.util.Date();		
				System.out.println(new Timestamp(date7.getTime())+" | recipes completed");
			}

			//			info.status=Status.Recipes_completed;



			//
			//
			//			//			info.status=Status.VMs_Prepared;
			//
			//
			//			//attributo del nodo che viene letto dalla ricetta
			//			String attribute= "{\"implementation_plan_id\":\""+databagId+"\"}";		
			//
			/////* the following part is commented since it works only on Amazon
			//			java.util.Date date4= new java.util.Date();	   
			//			System.out.println(new Timestamp(date4.getTime())+" | chef node bootstrapping");
			//
			//
			//			chefService.bootstrapChef(group, nodesInfo, cloudservice,attribute);
			//
			//			java.util.Date date5= new java.util.Date();	
			//			System.out.println(new Timestamp(date5.getTime())+" | chef node bootstrapped");
			//
			//			//			info.status=Status.Chef_Node_Bootstrapped;
			//
			////			for(ClusterNode node : nodes)  info.chefNodesName.add(group+"-"+node.getPrivateIP());
			////*/
			//			//---------------------Executing recipes on nodes ------------------------------------------
			//			for(int i=0;i<nodes.size();i++ ){
			//
			//				List<List<String>> recipes=new ArrayList<List<String>> ();
			//				List <String> temp = new ArrayList<String>();
			////				temp.add(plan.getPools().get(0).getVms().get(i).getComponents().get(0).getCookbook()
			////						+"::"+plan.getPools().get(0).getVms().get(i).getComponents().get(0).getRecipe());
			//				temp.add("applications::web-container-app");
			////				temp.add("specs-enforcement-webpool::nginx");
			//				recipes.add(temp);
			//
			//				java.util.Date date6= new java.util.Date();	
			//				System.out.println(new Timestamp(date6.getTime())+" | executing recipes: "+Arrays.toString(recipes.get(0).toArray())+" on node "+group+"-"+nodes.get(i).getPrivateIP()+" publicIP:"+nodes.get(i).getPublicIP());				
			//
			//				threads[i] = chefService.new ExecuteRecipesOnNode(nodes.get(i), recipes.get(0), group,myCred.getPrivatekey(), cloudservice);
			//				threads[i].start();
			//
			////				info.recipes.add(InfoRecipes(recipes.get(i)));
			//			}
			//
			//			for(int i=0;i<nodes.size();i++ ){
			//				try {
			//					threads[i].join();
			//				} catch (InterruptedException ignore) {}
			//			}
			//
			//			if(true){
			//				java.util.Date date7= new java.util.Date();		
			//				System.out.println(new Timestamp(date7.getTime())+" | recipes completed");
			//			}

			//			info.status=Status.Recipes_completed;

			// */
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
